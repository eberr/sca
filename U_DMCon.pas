unit U_DMCon;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Stan.Param, FireDAC.Comp.Client, Data.DB, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.IBBase, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.VCLUI.Login, FireDAC.Comp.DataSet;

type
  TDMCon = class(TDataModule)
    SCAConnection: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    qryUsuarios: TFDQuery;
    FDTransaction1: TFDTransaction;
    FDGUIxLoginDialog1: TFDGUIxLoginDialog;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMCon: TDMCon;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses U_Funcoes, Dialogs, IniFiles, IWSystem, SysUtils;

{$R *.dfm}

function ConectaBanco : Boolean;
var
  vArqIni : TIniFile;
  Caminho : string;
begin
  Caminho := 'C:\Projetos Delphi\SCA\Dados\SCA_DATABASE.FDB';
  DMCon.SCAConnection.Close;
  if not( FileExists(ExtractFilePath(ParamStr(0)) + 'sca.ini')) then
    begin
      ShowMessage('Arquivo de configura��es do Banco de Dados n�o encontrado!');
      vArqIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'sca.ini');
      try
        DMCon.SCAConnection.ConnectionName := 'SCA';
        vArqIni.WriteString('SCA','database', Caminho);
        DMCon.SCAConnection.Params.Values['DataBase'] := Caminho;
      finally
        vArqIni.Free;
      end;
  end;
  try
    vArqIni := TIniFile.Create(extractfilepath(ParamStr(0))+'Automacao.ini');
    DMCon.SCAConnection.ConnectionName := 'SCA';
    Caminho := vArqIni.ReadString('SCA','database',Caminho);
    DMCon.SCAConnection.Params.Values['DataBase'] := Caminho;
  finally
    FreeAndNil(vArqIni);
  end;
  try
    DMCon.SCAConnection.Connected := True;
    DMCon.qryUsuarios.Close;
    DMCon.qryUsuarios.Open;
   except
   end;
end;

procedure TDMCon.DataModuleCreate(Sender: TObject);
begin
 try
    with SCAConnection do
      begin
        Params.Clear;
        Params.Values['DriverID']  := 'FB';
        Params.Values['Server']    := TFuncoes.LerIni('FIREBIRD','Server');
        Params.Values['Database']  := TFuncoes.LerIni('FIREBIRD','Database');
        Params.Values['User_name'] := TFuncoes.LerIni('FIREBIRD','User');
        Params.Values['Password']  := TFuncoes.LerIni('FIREBIRD','Password');
        Connected := True;
      end;
  except
      msg ShowMessage('Ocorreu uma falha na configura��o no banco firebird!');
  end;
end;

end;

end.
