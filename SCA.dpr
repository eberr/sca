program SCA;

uses
  Vcl.Forms,
  U_DMCon in 'U_DMCon.pas' {DMCon: TDataModule},
  U_Funcoes in 'U_Funcoes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMCon, DMCon);
  Application.Run;
end.
