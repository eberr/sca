object DMCon: TDMCon
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 337
  Width = 593
  object SCAConnection: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Projetos Delphi\SCA\Dados\SCA_DATABASE.FDB'
      'DriverID=FB')
    Connected = True
    LoginDialog = FDGUIxLoginDialog1
    LoginPrompt = False
    Transaction = FDTransaction1
    Left = 56
    Top = 24
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 56
    Top = 80
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 56
    Top = 136
  end
  object qryUsuarios: TFDQuery
    DetailFields = 'CODIGO;DT_CADASTRO;ID_USUARIO;NOME'
    Connection = SCAConnection
    SQL.Strings = (
      'SELECT'
      '    ID_USUARIO'
      '   ,CODIGO'
      '   ,NOME'
      '   ,DT_CADASTRO'
      'FROM USUARIOS')
    Left = 160
    Top = 24
  end
  object FDTransaction1: TFDTransaction
    Connection = SCAConnection
    Left = 56
    Top = 184
  end
  object FDGUIxLoginDialog1: TFDGUIxLoginDialog
    Provider = 'Forms'
    Caption = 'SCA - Login'
    Left = 56
    Top = 240
  end
end
